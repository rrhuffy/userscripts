// ==UserScript==
// @name           Wykop plus filter
// @version        6
// @include        http*://*wykop.pl/*
// @downloadURL    https://bitbucket.org/rrhuffy/userscripts/raw/master/wykop.plus.filter.user.js
// ==/UserScript==

// based on Melcma's userscript: https://pastebin.com/dJGvEvLu
// based on Melcma's userscript "iOS banhammer"

const bar = document.querySelector('.nav.bspace.rbl-block ul') || document.querySelector('.nav.fix-b-border ul');

const filterInput = document.createElement('input');
filterInput.type = "number";
filterInput.id = "filterInput";
filterInput.min = 0;
filterInput.max = 100;
filterInput.style = "margin: 6px; width: 4em; background-color : #3c3c3c; border-color: #444; height: 2.4em;";
var initialValue = 0;

filterInput.setAttribute('value', initialValue);
filterInput.setAttribute('data-oldValue', initialValue);
filterInput.addEventListener('input', function (evt) {
  if (filterButton.classList.contains('active')) {
    restore();
    filter();
  }
  filterInput.setAttribute('data-oldValue', this.value);
});

bar.appendChild(filterInput);

const filterButton = document.createElement('BUTTON');
filterButton.innerHTML = 'filtruj';
filterButton.style.marginTop = '6px';
filterButton.addEventListener('click', handleClick);
bar.appendChild(filterButton);

function handleClick() {
  if (filterButton.classList.contains('active')) {
    return restore();
  }

  return filter();
}

function filter() {
  filterButton.classList.add('active');

  Array.prototype.forEach.call(document.getElementById('itemsStream').getElementsByClassName('author'), function (user) {
    // fix images (even on not touched elements because hiding is breaking lazy load calculations)
    Array.prototype.forEach.call(user.parentElement.parentElement.getElementsByTagName('img'), function (img) {
      if (img.classList.contains('lazy')) {
        if (img.getAttribute("data-original")) {
          img.src = img.getAttribute("data-original");
        }
      }
    });
    
    const vcElement = user.getElementsByClassName("vC")[0];
    const points = parseInt(vcElement.getAttribute("data-vC"));
    if (points >= document.getElementById("filterInput").value) {
      return;
    }

    const leftPart = '<span style="float: left; width:33.33%; text-align:left"><span style="position: relative; left: 35px">' + user.getElementsByTagName('a')[0].outerHTML + '</span></span>';
    const centerPart = '<span style="float: left; width:33.33%; text-align:center">Pokaż spoiler</span>';
    const rightPart = '<span style="float: left; width:33.33%; text-align:right"><span style="position: relative;left: -52px">' + vcElement.getElementsByTagName('b')[0].outerHTML + '</span></span>';
    
    const textOnSpoiler = leftPart + centerPart + rightPart + '&nbsp;'; // non breaking space to force proper height on this spoiler element

    const prefix = '<p class="showSpoiler" onmouseover="$(this).hide();$(this).next(\'code\').show().addClass(\'spoilerBody\')">' + textOnSpoiler + '</p><code class="dnone">';
    const postfix = '</code>';
    const textElement = user.closest('div[data-type="comment"') || user.closest('div[data-type="entrycomment"') || user.closest('div[data-type="entry"');
    
    textElement.innerHTML = prefix + textElement.innerHTML + postfix;
  });
}

function restore() {
  filterButton.classList.remove('active');
  const mouseoverEvent = new Event('mouseover');
  Array.prototype.forEach.call(document.getElementsByClassName('showSpoiler'), function (spoiler) {
    spoiler.dispatchEvent(mouseoverEvent);
  });
}

const loadMore = document.querySelector('.more .affect.ajax');

if (loadMore) {
  loadMore.addEventListener('click', () => {
    setTimeout(() => {
      filter();
    }, 1000)
  });
}

filter();