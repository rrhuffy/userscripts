// ==UserScript==
// @name           Video Speed Controller
// @version        7
// @grant          GM_getValue
// @grant          GM_setValue
// @include        http*://*.youtube.com/*
// @include        http*://*.twitch.tv/*
// @downloadURL    https://bitbucket.org/rrhuffy/userscripts/raw/master/video.speed.controller.user.js
// ==/UserScript==

// based on:
// https://github.com/codebicycle/videospeed/blob/firefox-port/inject.js
// https://github.com/mirnhoj/video-element-playbackrate-setter

// docs:
// https://www.freecodecamp.org/news/javascript-keycode-list-keypress-event-key-codes/

var currentPlaybackRate = 1;
var initialized = false;
var infobox = document.createElement("h4");
var requestedRate = 1;
var requestedChange = false;
var bufferInSeconds = 0;
var video;
var lastTime = null;
var savedTime = null;
var localSavedTime = 0;
var visible = true;
var debug = false;

initializeWhenReady(document);

function debugLog(message) {
  if (debug) {
    console.log(message);
  }
}

function formatTime(totalSeconds) {
    var totalSeconds = Math.floor(totalSeconds, 0);
    var seconds = totalSeconds % 60;
    var minutes = ~~(~~(totalSeconds % 3600) / 60);
    var hours = ~~(totalSeconds / 3600);
    if (totalSeconds < 60) { // 0-60s -> __s
        return `${seconds.toFixed(0)}s`
    } else if (totalSeconds < 3600) { // 0-60min -> __m:__s
        return `${minutes.toFixed(0)}m${seconds.toFixed(0)}s`;
    } else { // 60m+ -> __h:__m:__s
        return `${hours.toFixed(0)}h${minutes.toFixed(0)}m${seconds.toFixed(0)}s`;
    }
}

function updateTooltip() {
  if (!video) return;
  if (lastTime === null) {
    lastTime = video.currentTime;
  }

  if (lastTime > video.currentTime) {
    debugLog(`last time > current time: ${lastTime} > ${video.currentTime}, updating last time`);
    lastTime = video.currentTime;
  }

  if (savedTime === null) {
    savedTime = GM_getValue('savedTime', 0);
    debugLog(`GM_getValue("savedTime") returned ${savedTime}`);
  }

  var diff = video.currentTime - lastTime;

  if (video.paused) {
    lastTime = video.currentTime;
    return;
  }

  if (diff > 1) {
    debugLog(`diff is bigger than 1: ${diff}, probably rewind during play, ignoring`);
    lastTime = video.currentTime;
    return;
  }

  if (diff > 0) {
    var timeToAdd = diff * ((video.playbackRate-1)/video.playbackRate);
    // debugLog(`previous savedTime: ${savedTime} diff: ${diff} timeToAdd: ${timeToAdd}`);
    savedTime += timeToAdd;
    localSavedTime += timeToAdd;
    GM_setValue('savedTime', savedTime);
    // debugLog(`GM_setValue(savedTime, ${savedTime}) called`);
    lastTime = video.currentTime;
  } else {
    // debugLog(`diff is not positive: ${diff}`);
  }

  var remainingTime = video.duration - video.currentTime;
  var output = '';
  output += `${requestedRate.toFixed(2)}x (${video.playbackRate.toFixed(2)}) `; // 1.00x (1.00x) - requested/real playback speed
  output += `${bufferInSeconds.toFixed(2)}s  `; // 12.34s - buffered time
  output += ` (${formatTime(remainingTime)}->${formatTime(remainingTime / video.playbackRate)})`; // 40s -> 10s - remaining normal/speeded time
  output += ` [saved ${formatTime(savedTime)}, session: ${formatTime(localSavedTime)}]`;
  infobox.innerHTML = output;
}

function initializeVideoElement() {
  video = document.querySelector('video');
  console.log('Video element initialized: ' + video);
  if (!video) {
    setTimeout(initializeVideoElement, 1000);
  } else {
    initializeTooltip();
  }
}

function initializeTooltip() {
  // add infobox to dom if it doesn't already exist.
  if (video && !document.getElementById("playbackrate-indicator")) {
    infobox.setAttribute("id", "playbackrate-indicator");
    infobox.style.position = "absolute";
    infobox.style.top = "2%";
    infobox.style.left = "2%";
    infobox.style.color = "rgba(255, 0, 0, 1)";
    infobox.style.zIndex = "99999"; // ensures that it shows above other elements.
    infobox.style.visibility = "visible";
    infobox.style.position = "absolute";
    infobox.style.background = "black";
    infobox.style.color = "white";
    infobox.style.opacity = 0.8;
    infobox.style.cursor = "default";
    video.parentElement.appendChild(infobox);
  }
}

function updateBufferTime() {
  if (!video) return;

  var maxEnd = 0;
  var index;
  for (index = 0; index < video.buffered.length; ++index) {
    var thisEnd = video.buffered.end(index);
    if (thisEnd > maxEnd) {
      maxEnd = thisEnd;
    }
  }

  bufferInSeconds = (maxEnd - video.currentTime);
}

function updatePlaybackRate() {
  if (!video || !requestedChange) {
    return;
  }

  var needPause = (bufferInSeconds / requestedRate) < 0.5 || bufferInSeconds < 5 || video.readyState < 4;
  if (needPause && video.playbackRate > 1) {
    requestedRate = video.playbackRate; // save current speed for future to set it back (in a case when someone else changed spped, but buffer in unhealthy now)
    video.playbackRate = 1;
    requestedChange = true;
  } else if (video.readyState === 4 && !needPause) {
    video.playbackRate = requestedRate;
    debugLog('Set ' + requestedRate);
    setInterval(makeSureSpeedIsSetCorrectly, 100);
    requestedChange = false;
  }
}

function makeSureSpeedIsSetCorrectly() {
    if (video.playbackRate !== requestedRate) {
      debugLog('Tried to set ' + requestedRate + ' but it is ' + video.playbackRate + '. Trying one more time');
      video.playbackRate = requestedRate;
    }
}

function initializeWhenReady(document) {
  window.onload = () => {
    initializeNow(window.document);
  };
  if (document) {
    if (document.readyState === "complete") {
      initializeNow(document);
    } else {
      document.onreadystatechange = () => {
        if (document.readyState === "complete") {
          initializeNow(document);
        }
      };
    }
  }
}

function setPlaybackRate(rate) {
  // fix floating point errors like 1.1 + 0.1 = 1.2000000000000002.
  var newRoundedRate = Math.round(rate * 10) / 10;

  if (Math.abs(newRoundedRate - requestedRate) < 0.1) {
    // debugLog('setPlaybackRate(' + rate + '), but roundedRate - requestedRate: ' + Math.abs(newRoundedRate - requestedRate) + ', ignoring');
    return;
  }

  requestedRate = newRoundedRate;
  requestedChange = true;
  debugLog('setPlaybackRate(' + rate + ')');
}

function initializeNow(document) {
  if (initialized) {
    return;
  }
  document.addEventListener('keydown', function (event) {
    var keycode = event.charCode || event.keyCode;
    var changed = true;

    debugLog(`keycode: ${keycode} key: ${event.key} Alt: ${event.getModifierState("Alt") ? 'Y' : 'N'}`);

    if (event.getModifierState("Alt")) {
    switch (keycode) {
      case 96: currentPlaybackRate = Math.floor(currentPlaybackRate); break; //        !0
      case 97: currentPlaybackRate = Math.floor(currentPlaybackRate) + 0.1; break; //  !1
      case 98: currentPlaybackRate = Math.floor(currentPlaybackRate) + 0.2; break; //  !2
      case 99: currentPlaybackRate = Math.floor(currentPlaybackRate) + 0.3; break; //  !3
      case 100: currentPlaybackRate = Math.floor(currentPlaybackRate) + 0.4; break; // !4
      case 101: currentPlaybackRate = Math.floor(currentPlaybackRate) + 0.5; break; // !5
      case 102: currentPlaybackRate = Math.floor(currentPlaybackRate) + 0.6; break; // !6
      case 103: currentPlaybackRate = Math.floor(currentPlaybackRate) + 0.7; break; // !7
      case 104: currentPlaybackRate = Math.floor(currentPlaybackRate) + 0.8; break; // !8
      case 105: currentPlaybackRate = Math.floor(currentPlaybackRate) + 0.9; break; // !9
      default: changed = false;
      }
    } else {
      switch (keycode) {
        case 96:  currentPlaybackRate =10; break; // +0
        case 97:  currentPlaybackRate = 1; break; // +1
        case 98:  currentPlaybackRate = 2; break; // +2
        case 99:  currentPlaybackRate = 3; break; // +3
        case 100: currentPlaybackRate = 4; break; // +4
        case 101: currentPlaybackRate = 5; break; // +5
        case 102: currentPlaybackRate = 6; break; // +6
        case 103: currentPlaybackRate = 7; break; // +7
        case 104: currentPlaybackRate = 8; break; // +8
        case 105: currentPlaybackRate = 9; break; // +9
        case 106: visible = !visible; document.getElementById("playbackrate-indicator").style.visibility = visible ? "visible" : "hidden";break; // NumpadMul
        case 107: currentPlaybackRate = currentPlaybackRate + 0.1; break; //  NumPadAdd
        case 109: currentPlaybackRate = currentPlaybackRate - 0.1; break; //  NumPadSub
        default: debugLog("not using: " + keycode); changed = false;
      }
    }

    if (changed) {
      setPlaybackRate(currentPlaybackRate);

      // swallow event because we took care of it
      event.preventDefault();
      event.stopPropagation();
      return false;
    }
  }, true);

  setInterval(updateTooltip, 100);
  setInterval(updateBufferTime, 100);
  setInterval(updatePlaybackRate, 100);
  initializeVideoElement();
  initialized = true;
  console.log('Video Speed Controller initialized');
}